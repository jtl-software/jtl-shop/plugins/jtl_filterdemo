<?php

declare(strict_types=1);

namespace Plugin\jtl_filterdemo;

use JTL\DB\ReturnType;
use JTL\Filter\AbstractFilter;
use JTL\Filter\FilterInterface;
use JTL\Filter\Join;
use JTL\Filter\Option;
use JTL\Filter\ProductFilter;
use JTL\Filter\Query;
use JTL\Filter\StateSQL;
use JTL\Filter\Type;

/**
 * Class FilterORDemo
 * @package Plugin\jtl_filterdemo
 */
class FilterORDemo extends AbstractFilter
{
    /**
     * FilterORDemo constructor.
     *
     * @param ProductFilter $productFilter
     */
    public function __construct(ProductFilter $productFilter)
    {
        parent::__construct($productFilter);
        $this->setType(Type::OR)
            ->setTableName('xplugin_jtl_filterdemo_or')
            ->setUrlParam('dfor')
            ->setName('Demofilter OR')
            ->setFrontendName($this->getName());
    }

    /**
     * @inheritdoc
     */
    public function generateActiveFilterData(): FilterInterface
    {
        parent::generateActiveFilterData();
        // every active value would just be named "Demofilter OR" - so we just add ': <value>' to it
        foreach ($this->activeValues as $value) {
            $value->setFrontendName($value->getFrontendName() . ': ' . $value->getValue());
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setSeo(array $languages): FilterInterface
    {
        // TODO: Implement setSeo() method.
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryKeyRow(): string
    {
        return 'id';
    }

    /**
     * @inheritdoc
     */
    public function getSQLCondition(): string
    {
        return (new Query())
            ->setComment('condition from FilterORDemo::getSQLCondition()')
            ->setWhere($this->getTableName() . '.filterval IN ({value})')
            ->setParams(['value' => $this->getValue()])
            ->setType('IN')
            ->getSQL();
    }

    /**
     * @return array|int
     */
    public function getValue(): int|array
    {
        $value = parent::getValue();

        return \is_array($value) ? \array_map('\intval', $value) : (int)$value;
    }

    /**
     * @inheritdoc
     */
    public function getSQLJoin(): Join
    {
        return (new Join())
            ->setComment('join from FilterORDemo')
            ->setType('JOIN')
            ->setTable($this->getTableName())
            ->setOn('tartikel.kArtikel = ' . $this->getTableName() . '.kArtikel')
            ->setOrigin(__CLASS__);
    }

    /**
     * @param null $mixed
     * @return array
     */
    public function getOptions($mixed = null): array
    {
        if ($this->options !== null) {
            return $this->options;
        }
        $this->options = [];
        foreach ($this->getOptionData() as $option) {
            $this->options[] = (new Option())
                ->setURL(
                    $this->productFilter->getFilterURL()->getURL(
                        (new self($this->productFilter))->init((int)$option->filterval)
                    )
                )
                ->setType($this->getType())
                ->setClassName($this->getClassName())
                ->setParam($this->getUrlParam())
                ->setName($option->name)
                ->setValue((int)$option->filterval)
                ->setCount((int)$option->nAnzahl);
        }

        return $this->options;
    }

    /**
     * @return array
     */
    private function getOptionData(): array
    {
        $sql = (new StateSQL())->from($this->productFilter->getCurrentStateData($this->getClassName()));
        $sql->addJoin($this->getSQLJoin());
        $select = $this->getTableName() . '.filterval, ' . $this->getTableName() . '.name';

//        if (!empty($this->value) && count($this->value) > 0) {
//            $state->addJoin((new Join())
//                ->setTable($this->getTableName() . ' X')
//                ->setType('JOIN')
//                ->setOn('X.filterval = xplugin_jtl_filterdemo_or.filterval
//                            OR xplugin_jtl_filterdemo_or.filterval IN( ' . implode(',', $this->value) . ')')
//                ->setComment('join from FilterORDemo::getOptions()')
//                ->setOrigin(__CLASS__));
//            $select = 'DISTINCT X.filterval, X.name';
//            $state->setConditions(str_replace(
//                'xplugin_jtl_filterdemo_or.filterval IN',
//                'X.filterval NOT IN',
//                $state->getConditions()
//            ));
//        }

        $sql->setSelect([$select, 'tartikel.kArtikel']);
        $sql->setOrderBy('');

        return $this->productFilter->getDB()->query(
            'SELECT ford.filterval, ford.name, COUNT(*) AS nAnzahl
                FROM (' . $this->productFilter->getFilterSQL()->getBaseQuery($sql) . ' ) AS ford
                GROUP BY ford.filterval
                ORDER BY ford.filterval ASC',
            ReturnType::ARRAY_OF_OBJECTS
        );
    }
}
