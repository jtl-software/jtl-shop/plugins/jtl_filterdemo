<?php

declare(strict_types=1);

namespace Plugin\jtl_filterdemo;

use JTL\Filter\AbstractFilter;
use JTL\Filter\FilterInterface;
use JTL\Filter\ProductFilter;

/**
 * Class CustomBaseState
 * @package Plugin\jtl_filterdemo
 */
class CustomBaseState extends AbstractFilter
{
    /**
     * CustomBaseState constructor.
     * @param ProductFilter $productFilter
     */
    public function __construct(ProductFilter $productFilter)
    {
        parent::__construct($productFilter);
        $this->isCustom    = true;
        $this->urlParam    = 'qqqq';
        $this->urlParamSEO = null;
    }

    /**
     * @param array $languages
     * @return $this
     */
    public function setSeo(array $languages): FilterInterface
    {
        $this->cSeo = [];
        foreach ($languages as $language) {
            $this->cSeo[$language->kSprache] = 'Customfilter2';
        }

        return $this;
    }
}
