<?php

declare(strict_types=1);

namespace Plugin\jtl_filterdemo\Migrations;

use JTL\Plugin\Migration;
use JTL\Update\IMigration;

/**
 * Class Migration20181126120000
 * @package Plugin\jtl_filterdemo\Migrations
 * @noinspection PhpUnused
 */
class Migration20181126120000 extends Migration implements IMigration
{
    /**
     * @inheritdoc
     */
    public function up(): void
    {
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `xplugin_jtl_filterdemo_and` (
              `id`        INT(11)      NOT NULL AUTO_INCREMENT,
              `kArtikel`  INT(11)      NOT NULL,
              `filterval` INT(11)      NOT NULL,
              `name`      VARCHAR(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;'
        );
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `xplugin_jtl_filterdemo_or` (
              `id`        INT(11)      NOT NULL AUTO_INCREMENT,
              `kArtikel`  INT(11)      NOT NULL,
              `filterval` INT(11)      NOT NULL,
              `name`      VARCHAR(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;'
        );
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `xplugin_jtl_filterdemo_andseo` (
              `id`        INT(11)      NOT NULL AUTO_INCREMENT,
              `filterval` INT(11)      NOT NULL,
              `kSprache`  INT(11)      NOT NULL DEFAULT 1,
              `name`      VARCHAR(255) NOT NULL,
              `cSeo`      VARCHAR(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;'
        );
        $this->execute(
            'CREATE TABLE IF NOT EXISTS `xplugin_jtl_filterdemo_andseoassociations` (
              `id`        INT(11)      NOT NULL AUTO_INCREMENT,
              `kArtikel`  INT(11)      NOT NULL,
              `seoID`     INT(11)      NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;'
        );
        $this->execute('DELETE FROM xplugin_jtl_filterdemo_andseo');
        $this->execute('DELETE FROM xplugin_jtl_filterdemo_or');
        $this->execute('DELETE FROM xplugin_jtl_filterdemo_andseo');
        $this->execute('DELETE FROM xplugin_jtl_filterdemo_andseoassociations');
        $this->execute(
            "INSERT INTO xplugin_jtl_filterdemo_andseo (kSprache, filterval, name, cSeo)
                SELECT DISTINCT tsprache.kSprache, ORD(UPPER(SUBSTRING(tartikel.cSeo, 1, 1))), 
                              CONCAT('Buchstabe', UPPER(SUBSTRING(tartikel.cSeo, 1, 1)), tsprache.cISO),
                              CONCAT(UPPER(SUBSTRING(tartikel.cSeo, 1, 1)), tsprache.kSprache) FROM tartikel, tsprache;
            INSERT INTO xplugin_jtl_filterdemo_andseoassociations (kArtikel, seoID) 
                SELECT tartikel.kArtikel, xplugin_jtl_filterdemo_andseo.filterval 
                    FROM tartikel 
                    INNER JOIN xplugin_jtl_filterdemo_andseo 
                        ON xplugin_jtl_filterdemo_andseo.filterval = ORD(UPPER(SUBSTR(tartikel.cName, 1, 1)));
            INSERT INTO xplugin_jtl_filterdemo_or  (kArtikel, filterval) 
                SELECT kArtikel, (FLOOR(RAND() * (10 - 1 + 1)) + 1) FROM tartikel;
            INSERT INTO xplugin_jtl_filterdemo_and (kArtikel, filterval) 
                SELECT kArtikel, (FLOOR(RAND() * (10 - 1 + 1)) + 1) FROM tartikel;"
        );
        $this->execute(
            "UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Eins' WHERE filterval = 1;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Zwei' WHERE filterval = 2;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Drei' WHERE filterval = 3;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Vier' WHERE filterval = 4;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Fünf' WHERE filterval = 5;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Sechs' WHERE filterval = 6;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Sieben' WHERE filterval = 7;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Acht' WHERE filterval = 8;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Neun' WHERE filterval = 9;
            UPDATE `xplugin_jtl_filterdemo_or` SET `name`='Zehn' WHERE filterval = 10;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Eins' WHERE filterval = 1;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Zwei' WHERE filterval = 2;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Drei' WHERE filterval = 3;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Vier' WHERE filterval = 4;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Fünf' WHERE filterval = 5;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Sechs' WHERE filterval = 6;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Sieben' WHERE filterval = 7;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Acht' WHERE filterval = 8;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Neun' WHERE filterval = 9;
            UPDATE `xplugin_jtl_filterdemo_and` SET `name`='Zehn' WHERE filterval = 10;"
        );
    }

    /**
     * @inheritdoc
     */
    public function down(): void
    {
        $this->execute('DROP TABLE IF EXISTS `xplugin_jtl_filterdemo_and`');
        $this->execute('DROP TABLE IF EXISTS `xplugin_jtl_filterdemo_or`');
        $this->execute('DROP TABLE IF EXISTS `xplugin_jtl_filterdemo_andseo`');
        $this->execute('DROP TABLE IF EXISTS `xplugin_jtl_filterdemo_andseoassociations`');
    }
}
