<?php

declare(strict_types=1);

namespace Plugin\jtl_filterdemo;

use JTL\DB\ReturnType;
use JTL\Filter\AbstractFilter;
use JTL\Filter\FilterInterface;
use JTL\Filter\Join;
use JTL\Filter\Option;
use JTL\Filter\ProductFilter;
use JTL\Filter\StateSQL;
use JTL\Filter\Type;

/**
 * Class FilterANDDemo
 * @package Plugin\jtl_filterdemo
 */
class FilterANDDemo extends AbstractFilter
{
    /**
     * FilterANDDemo constructor
     *
     * @param ProductFilter $productFilter
     */
    public function __construct(ProductFilter $productFilter)
    {
        parent::__construct($productFilter);
        $this->setType(Type::AND)
            ->setUrlParam('dfand')
            ->setName('Demofilter AND')
            ->setFrontendName($this->getName());
    }

    /**
     * @inheritdoc
     */
    public function setSeo(array $languages): FilterInterface
    {
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryKeyRow(): string
    {
        return 'id';
    }

    /**
     * @inheritdoc
     */
    public function getTableName(): string
    {
        return 'xplugin_jtl_filterdemo_and';
    }

    /**
     * @inheritdoc
     */
    public function getSQLCondition(): string
    {
        return ' ' . $this->getTableName() . '.filterval = ' . $this->getValue();
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return (int)parent::getValue();
    }

    /**
     * @inheritdoc
     */
    public function getSQLJoin(): Join
    {
        return (new Join())
            ->setComment('join from FilterANDDemo')
            ->setType('JOIN')
            ->setTable($this->getTableName())
            ->setOn('tartikel.kArtikel = ' . $this->getTableName() . '.kArtikel')
            ->setOrigin(__CLASS__);
    }

    /**
     * @inheritdoc
     */
    public function generateActiveFilterData(): FilterInterface
    {
        parent::generateActiveFilterData();
        // every active value would just be named "Demofilter AND" - so we just add ': <value>' to it
        foreach ($this->activeValues as $value) {
            $value->setFrontendName($value->getFrontendName() . ': ' . $value->getValue());
        }

        return $this;
    }

    /**
     * @param null $mixed
     * @return array
     */
    public function getOptions($mixed = null): array
    {
        if ($this->options !== null) {
            return $this->options;
        }
        $this->options = [];
        foreach ($this->getOptionData() as $option) {
            $this->options[] = (new Option())
                ->setURL(
                    $this->productFilter->getFilterURL()->getURL(
                        (new self($this->productFilter))->init((int)$option->filterval)
                    )
                )
                ->setType($this->getType())
                ->setClassName($this->getClassName())
                ->setParam($this->getUrlParam())
                ->setName($option->name)
                ->setValue((int)$option->filterval)
                ->setCount((int)$option->nAnzahl);
        }

        return $this->options;
    }

    /**
     * @return array
     */
    private function getOptionData(): array
    {
        $sql = (new StateSQL())->from($this->productFilter->getCurrentStateData());
        $sql->addJoin($this->getSQLJoin());
        $sql->setSelect([$this->getTableName() . '.filterval', $this->getTableName() . '.name', 'tartikel.kArtikel']);
        $sql->setOrderBy('');

        return $this->productFilter->getDB()->query(
            'SELECT ssMerkmal.filterval, ssMerkmal.name, COUNT(*) AS nAnzahl
                FROM (' . $this->productFilter->getFilterSQL()->getBaseQuery($sql) . ' ) AS ssMerkmal
                GROUP BY ssMerkmal.filterval
                ORDER BY ssMerkmal.filterval ASC',
            ReturnType::ARRAY_OF_OBJECTS
        );
    }
}
