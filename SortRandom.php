<?php

declare(strict_types=1);

namespace Plugin\jtl_filterdemo;

use JTL\Filter\ProductFilter;
use JTL\Filter\SortingOptions\AbstractSortingOption;

/**
 * Class SortRandom
 * @package Plugin\jtl_filterdemo
 */
class SortRandom extends AbstractSortingOption
{
    /**
     * SortRandom constructor.
     * @param ProductFilter $productFilter
     */
    public function __construct(ProductFilter $productFilter)
    {
        parent::__construct($productFilter);
        $this->orderBy = 'RAND()';
        $this->setName(\__('Random'));
        $this->setPriority(12);
        $this->setValue(99);
    }
}
