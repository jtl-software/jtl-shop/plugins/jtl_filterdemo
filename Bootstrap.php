<?php

declare(strict_types=1);

namespace Plugin\jtl_filterdemo;

use JTL\Catalog\Product\Artikel;
use JTL\Catalog\Product\Preise;
use JTL\Events\Dispatcher;
use JTL\Filter\Config;
use JTL\Filter\FilterInterface;
use JTL\Filter\Items\Category as CategoryItem;
use JTL\Filter\Items\Characteristic;
use JTL\Filter\Items\Manufacturer as ManufacturerItem;
use JTL\Filter\Pagination\ItemFactory;
use JTL\Filter\Pagination\Pagination;
use JTL\Filter\ProductFilter;
use JTL\Filter\SortingOptions\Factory;
use JTL\Filter\States\BaseCategory;
use JTL\Helpers\Category;
use JTL\Link\LinkInterface;
use JTL\Plugin\Bootstrapper;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;

/**
 * Class Bootstrap
 * @package Plugin\jtl_filterdemo
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @param Dispatcher $dispatcher
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);
        $dispatcher->listen('shop.hook.' . \HOOK_PRODUCTFILTER_CREATE, [$this, 'hook252']);
        $dispatcher->listen('shop.hook.' . \HOOK_PRODUCTFILTER_REGISTER_SEARCH_OPTION, [$this, 'hook254']);
    }

    /**
     * @param array $args
     */
    public function hook252(array $args): void
    {
        if (Shop::getPageType() === \PAGE_STARTSEITE || Shop::getPageType() === \PAGE_AUSWAHLASSISTENT) {
            return;
        }
        $plugin        = $this->getPlugin();
        $productFilter = $args['productFilter'];
        /** @var ProductFilter $productFilter */
        if ($plugin->getConfig()->getValue('filter_and') === 'Y') {
            $productFilter->registerFilterByClassName(FilterANDDemo::class);
        }
        if ($plugin->getConfig()->getValue('filter_or') === 'Y') {
            $productFilter->registerFilterByClassName(FilterORDemo::class);
        }
        if ($plugin->getConfig()->getValue('filter_seo') === 'Y') {
            $productFilter->registerFilterByClassName(FilterSEODemo::class);
        }
    }

    /**
     * @param array $args
     */
    public function hook254(array $args): void
    {
        /** @var Factory $factory */
        $factory = $args['factory'];
        $factory->registerSortingOption(99, SortRandom::class);
    }

    /**
     * @inheritdoc
     */
    public function prepareFrontend(LinkInterface $link, JTLSmarty $smarty): bool
    {
        parent::prepareFrontend($link, $smarty);
        if ($link->getTemplate() === 'filter_frontend.tpl') {
            $pf = new ProductFilter(
                Config::getDefault(),
                $this->getDB(),
                $this->getCache()
            );
            $pf->setProductLimit(25);
            Category::getDataByAttribute(
                'kKategorie',
                1,
                static function ($category) use ($pf) {
                    return $category !== null
                        ? $pf->addActiveFilter(new CategoryItem($pf), (int)$category->kKategorie)
                        : false;
                }
            );
//            Manufacturer::getDataByAttribute(
//                'kHersteller',
//                1,
//                static function ($manufacturer) use ($pf) {
//                    return $manufacturer !== null
//                        ? $pf->addActiveFilter(new ManufacturerItem($pf), (int)$manufacturer->kHersteller)
//                        : false;
//                }
//            );
            $idx        = Frontend::getCustomerGroup()->getIsMerchant();
            $collection = $pf->generateSearchResults()->getProducts()
                ->each(static function ($a) use ($idx) {
                    /** @var Artikel $a */
                    $a->cName .= ' (' . $a->Preise->cVKLocalized[$idx] . ')';
                });

            $smarty->assign('customProducts', $collection)
                ->assign(
                    'avgPrice',
                    Preise::getLocalizedPriceString(
                        $collection->avg(static function (Artikel $a) {
                            return $a->Preise->fVKBrutto;
                        })
                    )
                );

            return true;
        }
        if ($link->getTemplate() === 'filter_frontend2.tpl') {
            $pf = (new ProductFilter(Config::getDefault(), $this->getDB(), $this->getCache()))
                ->initStates(Shop::getParameters())
                ->setProductLimit(12);
            $pf->setBaseState((new BaseCategory($pf))->init(1));
            $pf->setBaseState((new CustomBaseState($pf))->init(0));
            // only show attribute and manufacturer filters
            $pf->setAvailableFilters(
                \array_filter(
                    $pf->getAvailableFilters(),
                    static function (FilterInterface $f) {
                        return \in_array($f->getClassName(), [Characteristic::class, ManufacturerItem::class], true);
                    }
                )
            );
            $sr         = $pf->generateSearchResults();
            $pagination = new Pagination($pf, new ItemFactory());
            $pagination->create($sr->getPages());
            $smarty->assign('NaviFilter', $pf)
                ->assign('filterPagination', $pagination)
                ->assign('customSearchResults2', $sr);

            return true;
        }

        return false;
    }
}
