{block name='boxes-box-filter-characteristics'}
    {if $nSeitenTyp === $smarty.const.PAGE_ARTIKELLISTE
    && !($isMobile || $Einstellungen.template.productlist.filter_placement === 'modal')}
        {$filter = $NaviFilter->getFilterByClassName('Plugin\jtl_filterdemo\FilterORDemo')}
        {if $filter !== null}
            <div id="sidebox{$oBox->getID()}" class="box box-filter-custom filterdemo-or d-none d-lg-block">
                {button
                variant="link"
                class="btn-filter-box dropdown-toggle"
                block=true
                role="button"
                data=["toggle"=> "collapse", "target"=>"#cllps-box{$oBox->getID()}"]
                }
                    <span class="text-truncate">
                        OR!
                    </span>
                {/button}
                {collapse id="cllps-box{$oBox->getID()}"
                visible=$filter->isActive() || $Einstellungen.template.productlist.filter_items_always_visible === 'Y'}
                {block name='boxes-box-filter-manufacturer-include-manufacturer'}
                    {include file='snippets/filter/genericFilterItem.tpl' filter=$filter}
                {/block}
                {/collapse}
                {block name='boxes-box-filter-custom-hr'}
                    <hr class="box-filter-hr">
                {/block}
            </div>
        {/if}
    {/if}
{/block}
