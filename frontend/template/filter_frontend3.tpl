{if !empty($customProducts)}
    <div class="alert alert-success">
        <p>{$customProducts|count} Artikel gefunden.</p>
        <span>Hersteller: {$NaviFilter->HerstellerFilter->kHersteller}</span>
    </div>
    <ul class="list-group">
        {foreach $customProducts as $product}
            <li class="list-group-item">
                <a href="{$product->cURLFull}" title="{$product->cName}">{$product->cName}</a>
            </li>
        {/foreach}
    </ul>
{else}
    <div class="alert alert-info">
        <p>Keine Produkte gefunden.</p>
    </div>
{/if}
