{if count($NaviFilter->getSearchResultProducts()) > 0 && !isset($bAjaxRequest) || !$bAjaxRequest}
    <form id="improve_search" action="{$ShopURL}" method="get" class="form-inline clearfix">
        {include file='productlist/result_options.tpl'}
    </form>
{/if}
{block name="content"}
    <div id="result-wrapper">
        {assign var='style' value='gallery'}
        {assign var='grid' value='col-xs-6 col-lg-4'}
        {*Prio: -> Funktionsattribut -> Benutzereingabe -> Standarddarstellung*}
        {if (!empty($AktuelleKategorie->getCategoryFunctionAttribute('darstellung'))
        && $AktuelleKategorie->getCategoryFunctionAttribute('darstellung') == 1)
        || (empty($AktuelleKategorie->getCategoryFunctionAttribute('darstellung'))
        && ((!empty($oErweiterteDarstellung->nDarstellung)
        && isset($Einstellungen.artikeluebersicht.artikeluebersicht_erw_darstellung)
        && $Einstellungen.artikeluebersicht.artikeluebersicht_erw_darstellung === 'Y'
        && $oErweiterteDarstellung->nDarstellung == 1)
        || (empty($oErweiterteDarstellung->nDarstellung)
        && isset($Einstellungen.artikeluebersicht.artikeluebersicht_erw_darstellung_stdansicht)
        && $Einstellungen.artikeluebersicht.artikeluebersicht_erw_darstellung_stdansicht == 1)))}
            {assign var='style' value='list'}
            {assign var='grid' value='col-xs-12'}
        {/if}
        {if isset($customSearchResults2->Fehler)}
            <p class="alert alert-danger">{$customSearchResults2->Fehler}</p>
        {/if}
        {block name="productlist-results"}
            <div class="row {if $style !== 'list'}row-eq-height row-eq-img-height{/if} {$style}"
                 id="product-list"
                 itemprop="mainEntity"
                 itemscope itemtype="http://schema.org/ItemList">
                {foreach $customSearchResults2->getProducts() as $Artikel}
                    <div class="product-wrapper {$grid}" itemprop="itemListElement" itemscope
                         itemtype="http://schema.org/ListItem">
                        <meta itemprop="position" content="{$Artikel@iteration}">
                        {if $style === 'list'}
                            {include file='productlist/item_list.tpl' tplscope=$style}
                        {else}
                            {include file='productlist/item_box.tpl' tplscope=$style class='thumbnail'}
                        {/if}
                    </div>
                {/foreach}
            </div>
        {/block}
        {block name="productlist-footer"}
            {include file='productlist/footer.tpl'}
        {/block}
    </div>
{/block}
