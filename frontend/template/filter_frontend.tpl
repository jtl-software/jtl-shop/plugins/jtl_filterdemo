<div class="container">
    {if !empty($customProducts)}
        <div class="alert alert-success">
            <span>{$customProducts->count()} Artikel gefunden.</span>
        </div>
        <p>Durchschnittlicher Brutto-Preis: <strong>{$avgPrice}</strong></p>
        <br>
        <ul class="list-group">
            {foreach $customProducts as $product}
                <li class="list-group-item">
                    <a href="{$product->cURLFull}" title="{$product->cName}">{$product->cName}</a>
                </li>
            {/foreach}
        </ul>
    {else}
        <div class="alert alert-info">
            <p>Keine Produkte gefunden.</p>
        </div>
    {/if}
</div>
