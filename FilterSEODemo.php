<?php

declare(strict_types=1);

namespace Plugin\jtl_filterdemo;

use JTL\DB\ReturnType;
use JTL\Filter\AbstractFilter;
use JTL\Filter\FilterInterface;
use JTL\Filter\Join;
use JTL\Filter\Option;
use JTL\Filter\ProductFilter;
use JTL\Filter\StateSQL;
use JTL\Filter\Type;

/**
 * Class FilterSEODemo
 * @package Plugin\jtl_filterdemo
 */
class FilterSEODemo extends AbstractFilter
{
    /**
     * FilterSEODemo constructor
     *
     * @param ProductFilter $productFilter
     */
    public function __construct(ProductFilter $productFilter)
    {
        parent::__construct($productFilter);
        $this->setType(Type::AND)
            ->setUrlParam('dfseo')
            ->setUrlParamSEO('~')
            ->setName('Demofilter AND with SEO URLs')
            ->setFrontendName($this->getName());
    }

    /**
     * @param int|string $value
     * @return $this
     */
    public function setValue($value): FilterInterface
    {
        $this->value = (int)$value;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setSeo(array $languages): FilterInterface
    {
        $seo              = $this->productFilter->getDB()->selectAll(
            $this->getTableName(),
            ['filterval'],
            [$this->getValue()],
            'kSprache, cSeo, name'
        );
        $activeLanguageID = $this->productFilter->getFilterConfig()->getLanguageID();
        foreach ($languages as $language) {
            $this->cSeo[$language->kSprache] = '';
        }
        foreach ($seo as $item) {
            if ((int)$item->kSprache === $activeLanguageID && !empty($item->name)) {
                $this->setName($item->name);
            }
            $this->cSeo[(int)$item->kSprache] = $item->cSeo;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryKeyRow(): string
    {
        return 'filterval';
    }

    /**
     * @inheritdoc
     */
    public function getTableName(): string
    {
        return 'xplugin_jtl_filterdemo_andseo';
    }

    /**
     * @inheritdoc
     */
    public function getSQLCondition(): string
    {
        return ' ' . $this->getTableName() . '.filterval = ' . $this->getValue() .
            ' #condition from FilterSEODemo::getSQLCondition()' . "\n";
    }

    /**
     * @inheritdoc
     * @return Join[]
     */
    public function getSQLJoin(): array
    {
        return [
            (new Join())
                ->setComment('join1 from FilterSEODemo::getSQLJoin()')
                ->setType('JOIN')
                ->setTable('xplugin_jtl_filterdemo_andseoassociations')
                ->setOn('tartikel.kArtikel = xplugin_jtl_filterdemo_andseoassociations.kArtikel')
                ->setOrigin(__CLASS__),
            (new Join())
                ->setComment('join2 from FilterSEODemo::getSQLJoin()')
                ->setType('JOIN')
                ->setTable('xplugin_jtl_filterdemo_andseo')
                ->setOn('xplugin_jtl_filterdemo_andseo.filterval = xplugin_jtl_filterdemo_andseoassociations.seoID')
                ->setOrigin(__CLASS__)
        ];
    }

    /**
     * @param null $mixed
     * @return array
     */
    public function getOptions($mixed = null): array
    {
        if ($this->options !== null) {
            return $this->options;
        }
        $this->options = [];
        foreach ($this->getOptionData() as $option) {
            $this->options[] = (new Option())
                ->setURL(
                    $this->productFilter->getFilterURL()->getURL(
                        (new self($this->productFilter))->init((int)$option->filterval)
                    )
                )
                ->setType($this->getType())
                ->setClassName($this->getClassName())
                ->setParam($this->getUrlParam())
                ->setName($option->name)
                ->setValue((int)$option->filterval)
                ->setCount((int)$option->cnt);
        }

        return $this->options;
    }

    /**
     * @return array
     */
    private function getOptionData(): array
    {
        $state = $this->productFilter->getCurrentStateData();
        $table = $this->getTableName();
        foreach ($this->getSQLJoin() as $join) {
            $state->addJoin($join);
        }
        $state->addCondition('xplugin_jtl_filterdemo_andseo.kSprache = ' . $this->getLanguageID());

        $sql = new StateSQL();
        $sql->setJoins($state->getJoins());
        $sql->setSelect([$table . '.filterval', $table . '.name', $table . '.cSeo', 'tartikel.kArtikel']);
        $sql->setConditions($state->getConditions());
        $sql->setHaving($state->getHaving());
        $sql->setOrderBy('');

        return $this->productFilter->getDB()->query(
            'SELECT ssMerkmal.filterval, ssMerkmal.name, ssMerkmal.cSeo, COUNT(*) AS cnt
                FROM (' . $this->productFilter->getFilterSQL()->getBaseQuery($sql) . ' ) AS ssMerkmal
                GROUP BY ssMerkmal.cSeo
                ORDER BY ssMerkmal.cSeo ASC',
            ReturnType::ARRAY_OF_OBJECTS
        );
    }
}
